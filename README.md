# README #

This is the sourcecode for the TrainFES iOS Application. The application is compatible with iOS8 and iOS 9. It connects to the TrainFES ElectroStimulators using a Bluetooth 4.0/BLE connections over RFduino. It includes a forked version of the RFduino library. 

### Details ###

* Share sourcecode with Team TrainFES
* 1.2
* [iTunes Store Link soon to Come]

### How to install ###

* No steps for now.

### Who do I talk to? ###

* Cristian Duguet
* TrainFES team www.trainfes.com