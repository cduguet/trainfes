//
//  PulseViewController.swift
//  TrainFES
//
//  Created by Cristian Duguet on 09/10/2015
//  Copyright (c) 2015 Cristian Duguet. All rights reserved.
//

import UIKit
import Sapporo

enum PulseHeaderType: String {
    case Second    = "SecondHeaderView"
    case Channel   = "ChannelHeaderView"
}
enum GridLineType: String {
    case Vertical = "VerticalGridLineView"
    case Horizontal = "HorizontalGridLineView"
}

var cornerSize = 40 as CGFloat
var totalTime = 0 as Int
var delayTime = 1

class PulseViewController: UIViewController, UIGestureRecognizerDelegate, RFduinoDelegate {
    
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var finishSlider: UISlider!
    @IBOutlet var pulseNameLabel: UILabel!
    @IBOutlet var startTimeLabel: UILabel!
    @IBOutlet var durationLabel: UILabel!
    @IBOutlet var totalTimeLabel: UILabel!
    
    @IBOutlet var leftSlopeButton: UIButton!
    @IBOutlet var rightSlopeButton: UIButton!
    @IBOutlet var channelLabel: UILabel!
    
    // For making sure that the subviews are within the insets delimited by navigation bar and status bar:
    override func viewDidLayoutSubviews() {
        let top = self.topLayoutGuide.length
        let bottom = self.bottomLayoutGuide.length
        //print("Subview - top: \(top), inset: \(self.collectionView.contentInset.top)")
        if self.collectionView.contentInset.top != top {
            let newInsets = UIEdgeInsetsMake(top, 0, bottom, 0)
            self.collectionView.contentInset = newInsets
        }
    }
    lazy var sapporo: Sapporo = { [unowned self] in
        return Sapporo(collectionView: self.collectionView)
        }()
    
    
    override func viewWillDisappear(animated: Bool) {
        self.tabBarController?.tabBar.hidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.hidden = true
        
        sapporo.delegate = self
        sapporo.registerNibForClass(PulseEventCell)
        sapporo.registerNibForSupplementaryClass(PulseHeaderView.self, kind: PulseHeaderType.Second.rawValue)
        sapporo.registerNibForSupplementaryClass(PulseHeaderView.self, kind: PulseHeaderType.Channel.rawValue)        
//        sapporo.registerNibForSupplementaryClass(FinishLineView.self, kind: FinishLineType)
        
        let layout = PulseLayout()
        layout.registerClass(GridLineView.self, forDecorationViewOfKind: GridLineType.Vertical.rawValue)
        layout.registerClass(GridLineView.self, forDecorationViewOfKind: GridLineType.Horizontal.rawValue)
        sapporo.setLayout(layout)
        
        finishSlider.layer.zPosition = 100
        finishSlider.frame = CGRect(x: layout.ChannelHeaderWidth, y: finishSlider.frame.origin.y, width: finishSlider.frame.width - layout.ChannelHeaderWidth, height: finishSlider.frame.height)
        

        let randomEvent = { () -> PulseEvent in
            let randomID = arc4random_uniform(10000)
            let title = "\(randomID)"
            
            let randomChannel = Int(arc4random_uniform(6))
            let randomStartSecond = CGFloat(arc4random_uniform(8))
            let randomDuration = CGFloat(arc4random_uniform(2) + 1)
            
            return PulseEvent(title: title, channel: randomChannel, startSecond: randomStartSecond, durationInSeconds: randomDuration)
        }
        
        let cellmodels = (0...2).map { _ -> PulseEventCellModel in
            let event = randomEvent()
            let cellModel = PulseEventCellModel(event: event) { (cell) -> Void in
                //print("Initial time: \(event.startSecond)s, duration: \(event.durationInSeconds)s")
                
                // change labels when selected cell
                let formatter = NSNumberFormatter()
                formatter.numberStyle = .DecimalStyle
                formatter.maximumFractionDigits = 2
                
                self.startTimeLabel.text = formatter.stringFromNumber(event.startSecond)! + "s"
                self.durationLabel.text = formatter.stringFromNumber(event.durationInSeconds)! + "s"
                self.channelLabel.text = "\(event.channel + 1)"
                self.updateSlopes(event)
                print("selecting event:" + event.title)
                cell.backgroundColor = UIColor(red: 58/255, green: 141/255, blue: 225/255, alpha: 1.0)
            }
            cellModel.deselectHandler = {(cell) -> Void in
                cell.backgroundColor = UIColor(red: 164/255, green: 215/255, blue: 255/255, alpha: 0.7)
            }
            return cellModel
        }
        
        print("No. of cellmodels: \(cellmodels.count)")
        
        self.collectionView.scrollEnabled   = false
        self.collectionView.pagingEnabled   = false
        
        sapporo[0].append(cellmodels)
        sapporo.bump()
        
        setupGestureRecognizer()
        
        totalTimeLabel.text = "\(finishSlider.value)s"
    }
    
    override func viewWillAppear(animated: Bool) {
        UIView.setAnimationsEnabled(false)
    }
    override func viewDidAppear(animated: Bool) {
        let value = UIInterfaceOrientation.LandscapeLeft.rawValue
        UIDevice.currentDevice().setValue(value, forKey: "orientation")
        UIView.setAnimationsEnabled(true)
    }
    
    //For moving a cell
    
    struct Bundle {
        var offset : CGPoint = CGPointZero
        var sourceCell : PulseEventCell?
        var representationImageView : UIView?
        var currentIndexPath : NSIndexPath
        var canvas : UIView
        var initialDuration : CGFloat?
        var initialPinch : CGFloat?
    }
    var bundle : Bundle?
    
    var canvas : UIView? {
        didSet {
            if canvas != nil {
                // self.calculateBorders()
            }
        }
    }
    
    func setupGestureRecognizer() {
        if let collectionView = self.collectionView {
            
            let longPressGestureRecogniser = UILongPressGestureRecognizer(target: self, action: "handleLongPress:")
            longPressGestureRecogniser.minimumPressDuration = 0.2
            longPressGestureRecogniser.delegate = self
            collectionView.addGestureRecognizer(longPressGestureRecogniser)
            
            let pinchGestureRecognizer = UIPinchGestureRecognizer(target: self, action: "handlePinch:")
            pinchGestureRecognizer.delegate = self
            collectionView.addGestureRecognizer(pinchGestureRecognizer)

            let doubleTapGestureRecognizer = UITapGestureRecognizer(target: self, action: "handleDoubleTap:")
            doubleTapGestureRecognizer.numberOfTapsRequired  = 2
            collectionView.addGestureRecognizer(doubleTapGestureRecognizer)
            
            
            if self.canvas == nil {
                self.canvas = self.collectionView!.superview
                
            }
        }
    }
    
    
    func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
        if let ca = self.collectionView!.superview {
            if let cv = self.collectionView {
                let pointPressedInCanvas = gestureRecognizer.locationInView(ca)
                for cell in cv.visibleCells() as! [PulseEventCell] {
                    let cellInCanvasFrame = ca.convertRect(cell.frame, fromView: cv)
                    //print("Gesture: \(gestureRecognizer)")
                    if CGRectContainsPoint(cellInCanvasFrame, pointPressedInCanvas ) {
                        if gestureRecognizer.isKindOfClass(UILongPressGestureRecognizer) {
                            let representationImage = cell.snapshotViewAfterScreenUpdates(true)
                            representationImage.frame = cellInCanvasFrame
                            let offset = CGPointMake(pointPressedInCanvas.x - cellInCanvasFrame.origin.x, pointPressedInCanvas.y - cellInCanvasFrame.origin.y)
                            let indexPath : NSIndexPath = cv.indexPathForCell(cell as UICollectionViewCell)!
                            self.bundle = Bundle(offset: offset, sourceCell: cell, representationImageView:representationImage, currentIndexPath: indexPath, canvas: ca, initialDuration: nil, initialPinch: nil)
                            break
                            
                        }
                       
                        if gestureRecognizer.isKindOfClass(UIPinchGestureRecognizer) {
                            let indexPath : NSIndexPath = cv.indexPathForCell(cell as UICollectionViewCell)!
                            self.bundle = Bundle(offset: CGPointMake(0, 0), sourceCell: cell, representationImageView: nil, currentIndexPath: indexPath, canvas: ca, initialDuration: (cell.cellmodel as! PulseEventCellModel).event.durationInSeconds, initialPinch: 0)
                            //print("bundle: \(self.bundle!)")
                            break
                        }
                    }
                }
                if gestureRecognizer.isKindOfClass(UIPinchGestureRecognizer) {
                    let indexPaths : NSArray = self.collectionView!.indexPathsForSelectedItems()!
                    if indexPaths.count == 1  {
                        if let indexPath : NSIndexPath = indexPaths[0] as? NSIndexPath {
                            if let cellModel = sapporo[indexPath] as? PulseEventCellModel {
                                self.bundle = Bundle(offset: CGPointMake(0, 0), sourceCell: nil, representationImageView: nil, currentIndexPath: indexPath, canvas: ca, initialDuration: cellModel.event.durationInSeconds, initialPinch: 0)
                            }
                        }
                    }
                }
            }
        }
        return (self.bundle != nil)
    }

    func handleDoubleTap(gesture: UITapGestureRecognizer) -> Void {
        if let _ = self.collectionView!.superview {
            if let cv = self.collectionView {
                //print("Main canvas: \(ca)")
                //print("Collection View Canvas: \(cv)")
                //let pointPressedInCanvas = gesture.locationInView(ca)
                let pointPressedInScreen = gesture.locationInView(cv)
                for cell in cv.visibleCells() as! [PulseEventCell] {
                    //let cellInCanvasFrame = ca.convertRect(cell.frame, fromView: cv)
                    let cellInScreenFrame = cell.frame
                    if CGRectContainsPoint(cellInScreenFrame, pointPressedInScreen ) {
                        let indexPath : NSIndexPath = cv.indexPathForCell(cell as UICollectionViewCell)!
                        sapporo[0].remove(indexPath.row)
                        collectionView.collectionViewLayout.invalidateLayout()
                        
                        //clear labels
                        channelLabel.text = ""
                        startTimeLabel.text = ""
                        durationLabel.text = ""
                        return
                    }
                }
                // create new cell 
                let layout = collectionView.collectionViewLayout as! PulseLayout
                let channel = layout.channelIndexFromYCoordinate(pointPressedInScreen.y)
                let startSecond = layout.secondIndexFromXCoordinate(pointPressedInScreen.x)
                let randomID = arc4random_uniform(10000)
                let title = "\(randomID)"
                let newEvent = PulseEvent(title: title, channel: channel, startSecond: startSecond, durationInSeconds: 1.0)
                
                let newCellModel = PulseEventCellModel(event: newEvent) { (cell) -> Void in
                    //print("Initial time: \(event.startSecond)s, duration: \(event.durationInSeconds)s")
                    
                    // change labels when selected cell
                    let formatter = NSNumberFormatter()
                    formatter.numberStyle = .DecimalStyle
                    formatter.maximumFractionDigits = 2
                    
                    self.startTimeLabel.text = formatter.stringFromNumber(newEvent.startSecond)! + "s"
                    self.durationLabel.text = formatter.stringFromNumber(newEvent.durationInSeconds)! + "s"
                    self.channelLabel.text = "\(newEvent.channel + 1)"
                    self.updateSlopes(newEvent)
                    print("selecting event:" + newEvent.title)
                    cell.backgroundColor = UIColor(red: 58/255, green: 141/255, blue: 225/255, alpha: 1.0)
                }
                newCellModel.deselectHandler = {(cell) -> Void in
                    cell.backgroundColor = UIColor(red: 164/255, green: 215/255, blue: 255/255, alpha: 0.7)
                }
                
                sapporo[0].append(newCellModel)
                sapporo.bump()
                
                channelLabel.text = "\(channel + 1)"
                let formatter = NSNumberFormatter()
                formatter.numberStyle = .DecimalStyle
                formatter.maximumFractionDigits = 2
                startTimeLabel.text = formatter.stringFromNumber(startSecond)! + "s"
                durationLabel.text = "1.0s"
                updateSlopes(newEvent)
            }
        }
    }

    func handlePinch(gesture: UIPinchGestureRecognizer ) -> Void {
        if gesture.numberOfTouches() != 2 {return}
        //let sCell = self.bundle!.sourceCell! as PulseEventCell
        
        
        if gesture.state == .Began {
            // Get the pinch points.
            let p1 = gesture.locationOfTouch(0, inView: collectionView)
            let p2 = gesture.locationOfTouch(1, inView: collectionView)
            
            let layout = collectionView.collectionViewLayout as! PulseLayout
            
            let w = layout.layoutAttributesForItemAtIndexPath(bundle!.currentIndexPath).frame.width
            
            bundle!.initialPinch = abs(p2.x - p1.x) / w
            
            // change labels
            let cellModel = sapporo[self.bundle!.currentIndexPath] as? PulseEventCellModel
            let formatter = NSNumberFormatter()
            formatter.numberStyle = .DecimalStyle
            formatter.maximumFractionDigits = 2
            startTimeLabel.text = formatter.stringFromNumber(cellModel!.event.startSecond)! + "s"
            channelLabel.text = "\(cellModel!.event.channel + 1)"
            updateSlopes(cellModel!.event)
        }

        
        if gesture.state == .Changed {
            print("pinch")
            //var attributes = UICollectionViewLayoutAttributes(forCellWithIndexPath: bundle.currentIndexPath)
            //var layout = collectionView.collectionViewLayout
            
            let cellModel = sapporo[self.bundle!.currentIndexPath] as? PulseEventCellModel
            if  cellModel != nil {
//                let p1 = gesture.locationOfTouch(0, inView: collectionView)
//                let p2 = gesture.locationOfTouch(1, inView: collectionView)
                //let layout = collectionView.collectionViewLayout as! PulseLayout
//                let w = layout.layoutAttributesForItemAtIndexPath(bundle!.currentIndexPath).frame.width
                //let newPinch = abs(p1.x - p2.x) / w
                
                //print(gesture.scale)
                
                print("duration original: \(bundle!.initialDuration)")
                print("scale: \(gesture.scale)")
                cellModel!.event.durationInSeconds = max(bundle!.initialDuration! + gesture.scale *  bundle!.initialPinch!, 0.1)
//                cellModel!.event.durationInSeconds = max(bundle!.initialDuration! + newPinch - bundle!.initialPinch!, 0.1)
                
                let formatter = NSNumberFormatter()
                formatter.numberStyle = .DecimalStyle
                formatter.maximumFractionDigits = 2
                
                self.durationLabel.text = formatter.stringFromNumber(cellModel!.event.durationInSeconds)! + "s"
                
                let animationsEnabled : Bool = UIView.areAnimationsEnabled()
                UIView.setAnimationsEnabled(false)
                collectionView.reloadItemsAtIndexPaths([cellModel!.indexPath])
                UIView.setAnimationsEnabled(animationsEnabled)
            }
            
        }
        if gesture.state == .Ended {
            collectionView.selectItemAtIndexPath(bundle!.currentIndexPath, animated: false, scrollPosition: UICollectionViewScrollPosition.None)
            sapporo.collectionView(collectionView, didSelectItemAtIndexPath: bundle!.currentIndexPath)
            self.bundle = nil
        }
    }
    
    
    func handleLongPress(gesture: UILongPressGestureRecognizer) -> Void {
        
        if let bundle = self.bundle {
            let cell = sapporo[self.bundle!.currentIndexPath] as? PulseEventCellModel

            //let sCell = self.bundle!.sourceCell! as PulseEventCell
            let dragPointOnCanvas = gesture.locationInView(self.collectionView!.superview)
            
            if gesture.state == UIGestureRecognizerState.Began {
                bundle.sourceCell!.hidden = true
                self.canvas?.addSubview(bundle.representationImageView!)
                UIView.animateWithDuration(0.5, animations: { () -> Void in
                    bundle.representationImageView!.alpha = 0.8
                });
                print("Dragging element with Path:")
                print(bundle.currentIndexPath.row)
                
                
                // change labels
                let formatter = NSNumberFormatter()
                formatter.numberStyle = .DecimalStyle
                formatter.maximumFractionDigits = 2
                startTimeLabel.text = formatter.stringFromNumber(cell!.event.startSecond)! + "s"
                channelLabel.text = "\(cell!.event.channel + 1)"
                durationLabel.text = formatter.stringFromNumber(cell!.event.durationInSeconds)! + "s"
                updateSlopes(cell!.event)
            }
            
            
            if gesture.state == UIGestureRecognizerState.Changed {
                // Update the representation image
                var imageViewFrame = bundle.representationImageView!.frame
                var point = CGPointZero
                point.x = dragPointOnCanvas.x - bundle.offset.x
                point.y = dragPointOnCanvas.y - bundle.offset.y
                imageViewFrame.origin = point
                bundle.representationImageView!.frame = imageViewFrame
                //let dragPointOnCollectionView = gesture.locationInView(self.collectionView)
                
                let formatter = NSNumberFormatter()
                formatter.numberStyle = .DecimalStyle
                formatter.maximumFractionDigits = 2
                let layout = collectionView.collectionViewLayout as! PulseLayout
                
                self.startTimeLabel.text = formatter.stringFromNumber(layout.secondIndexFromXCoordinate(point.x))! + "s"
                self.channelLabel.text = formatter.stringFromNumber(layout.channelIndexFromYCoordinate(point.y) + 1)!
            }
            
            if gesture.state == UIGestureRecognizerState.Ended {
                //var attributes = UICollectionViewLayoutAttributes(forCellWithIndexPath: bundle.currentIndexPath)
                
                let cellModel = sapporo[bundle.currentIndexPath] as? PulseEventCellModel
                if  cellModel != nil {
                    //var event = cellModel!.event
                    let loc = gesture.locationInView(self.collectionView).x
                    let layout = collectionView.collectionViewLayout as! PulseLayout
                    
                    cellModel!.event.startSecond =
                        max (layout.secondIndexFromXCoordinate(loc - bundle.offset.x), 0.00001)
                    cellModel!.event.channel = layout.channelIndexFromYCoordinate(gesture.locationInView(self.collectionView).y)
                    
                    // update labels
                    let formatter = NSNumberFormatter()
                    formatter.numberStyle = .DecimalStyle
                    formatter.maximumFractionDigits = 2
                    startTimeLabel.text = formatter.stringFromNumber(cellModel!.event.startSecond)! + "s"
                    //durationLabel.text = formatter.stringFromNumber(cellModel!.event.durationInSeconds)
                    channelLabel.text = "\(cellModel!.event.channel + 1)"
                    //cellModel!.bump()
                }
                bundle.representationImageView!.removeFromSuperview()
                
                if let _ = self.collectionView!.delegate as UICollectionViewDelegate? { // if we have a proper data source then we can reload and have the data displayed correctly
                    
                    dispatch_async(dispatch_get_main_queue()) {self.collectionView.reloadData()}
                    bundle.sourceCell!.hidden = false
                }
                self.bundle = nil
            }
        }
    }
    
    // ************************************** Slider ***********************************
    
    
    @IBAction func finishSliderChanged(sender: AnyObject) {
        let formatter = NSNumberFormatter()
        formatter.numberStyle = .DecimalStyle
        formatter.maximumFractionDigits = 2
        totalTime = Int(finishSlider.value)
        totalTimeLabel.text = formatter.stringFromNumber(finishSlider.value)! + "s"
    }
    
    // **************************************** Slope Buttons *********************************
    
    @IBAction func leftSlopeToggle(sender: AnyObject) {
        let indexPaths : NSArray = self.collectionView!.indexPathsForSelectedItems()!
        if indexPaths.count != 1  {
            print("ERROR: There are \(indexPaths.count) items selected")
            return
        }
        
        if let indexPath : NSIndexPath = indexPaths[0] as? NSIndexPath {
            if let cellModel = sapporo[indexPath] as? PulseEventCellModel {
                if cellModel.event.leftSlope { cellModel.event.leftSlope = false }
                else { cellModel.event.leftSlope = true }
                updateSlopes(cellModel.event)
            }
        }
    }
    
    @IBAction func rightSlopeToggle(sender: AnyObject) {
        let indexPaths : NSArray = self.collectionView!.indexPathsForSelectedItems()!
        if indexPaths.count != 1  {
            print("ERROR: There are zero or more than one item selected")
            return
        }
        
        if let indexPath : NSIndexPath = indexPaths[0] as? NSIndexPath {
            if let cellModel = sapporo[indexPath] as? PulseEventCellModel {
                if cellModel.event.rightSlope { cellModel.event.rightSlope = false }
                else { cellModel.event.rightSlope = true }
                updateSlopes(cellModel.event)
            }
        }
    }
    
    func updateSlopes(event: PulseEvent) {
        if event.leftSlope {
            let p = CGPointMake(leftSlopeButton.bounds.maxX, leftSlopeButton.bounds.maxY)
            let maskPath = UIBezierPath(arcCenter: p, radius: leftSlopeButton.bounds.width, startAngle: 0.0, endAngle: -90, clockwise: false)
            //var maskPath = UIBezierPath(roundedRect: leftSlopeButton.bounds, byRoundingCorners: UIRectCorner.TopLeft, cornerRadii: CGSizeMake(cornerSize, cornerSize))
            let maskLayer = CAShapeLayer()
            maskLayer.frame = leftSlopeButton.bounds
            maskLayer.path  = maskPath.CGPath
            leftSlopeButton.layer.mask = maskLayer
        } else {
            leftSlopeButton.layer.mask = nil
        }
        
        if event.rightSlope {
            let p = CGPointMake(rightSlopeButton.bounds.minX, rightSlopeButton.bounds.maxY)
            let maskPath = UIBezierPath(arcCenter: p, radius: rightSlopeButton.bounds.width, startAngle: 0.0, endAngle: 90, clockwise: true)
            //var maskPath = UIBezierPath(roun  dedRect: rightSlopeButton.bounds, byRoundingCorners: UIRectCorner.TopRight, cornerRadii: CGSizeMake(cornerSize, cornerSize))
            let maskLayer = CAShapeLayer()
            maskLayer.frame = rightSlopeButton.bounds
            maskLayer.path  = maskPath.CGPath
            rightSlopeButton.layer.mask = maskLayer
        } else {
            rightSlopeButton.layer.mask = nil
        }
    }
    
    // ************************************************ Print Pulses *********************************************
    // go for each channel and merge pulses
    
    @IBAction func printButton(sender: AnyObject) {
        printPulses()
    }
    
    func printPulses() {
        // Send configuration Strings
        
        var configStrings = [String]()
        for var i=0; i<6; i++ {
            configStrings.append("_A\(i+1);\(intensities[i]);\(pulseWidth);\(frequency);\(totalTime)\n")
            print(configStrings.last!)
            sendString(configStrings.last!)
        }
        
        // mode 1: this mode sends the pulses only by indexPath order
        var pulseStrings = [String]()
        for cell in collectionView.visibleCells() as! [PulseEventCell] {
            let e = (cell.cellmodel as! PulseEventCellModel).event
            
            pulseStrings.append("_B\(e.channel + 1)" +
                ";\(Int(e.startSecond * 10))" +
                ";\(Int(e.durationInSeconds * 10))" +
                ";\(Int(finishSlider.value * 10))" +
                (e.leftSlope || e.rightSlope ? ";\(leftSlopeDuration);\(rightSlopeDuration)" : "") +
            "\n")
            print(pulseStrings.last!)
            sendString(pulseStrings.last!)
            
            print("_E\n")
            sendString("_E\n")
        }
        
        
    }
    
    // MARK: RFDuino Functions
    
    func sendByte(byte: UInt8) {
        var tx: [UInt8] = [byte]
        let data = NSData(bytes: &tx, length: sizeof(UInt8))
        do {
            try rfduino.send(data)
        } catch _ {
            
        }
        
    }
    
    func sendString(str: String) {
        var byteArray = [UInt8]()
        for char in str.utf8{
            byteArray += [char]
        }
        let data = NSData(bytes: &byteArray, length: byteArray.count)
        
        do {
            try rfduino.send(data)
        } catch _ {
        }
    }
    
    func disconnect(sender: String) {
        print("Disconnecting...")
        rfduino.disconnect()
    }
    
    func didReceive(data: NSData!) {
        print("Received Data")
        //        var value : [UInt8] = [0]; data.getBytes(&value, length: 1)
        //        var len = data.length
        //
        //        if (value[0] == 1) { image1.image = UIImage(named: "on")}
        //        else { image1.image = UIImage(named: "off")}
    }

    
}

extension PulseViewController: SapporoDelegate{
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        if kind == PulseHeaderType.Second.rawValue {
            let view = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: PulseHeaderView.reuseIdentifier, forIndexPath: indexPath) as! PulseHeaderView
            view.titleLabel.text = "\(indexPath.item)"
            return view
        } else /*if kind == PulseHeaderType.Channel.rawValue */ {
            let view = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: PulseHeaderView.reuseIdentifier, forIndexPath: indexPath) as! PulseHeaderView
            view.titleLabel.text = "Channel \(indexPath.item + 1)"
            return view
        }
    }
}

