//
//  PulseLayout.swift
//
//
//  Created by Cristian Duguet on 8/28/15.
//  Copyright (c) 2015 Cristian Duguet. All rights reserved.
//

// http://www.objc.io/issues/3-views/collection-view-layouts

import UIKit
import Sapporo



class PulseLayout: SALayout, UIGestureRecognizerDelegate {
    let SecondsMaximum      : CGFloat = 10
    let ChannelsMaximum     : CGFloat = 6
    let HorizontalSpacing   : CGFloat = 10
    //let WidthPerSecond      : CGFloat = 100
    let SecondHeaderHeight  : CGFloat = 25
    let ChannelHeaderWidth     : CGFloat = 100
    let VerticalSpacing     : CGFloat = 4

    let rightPadding        : CGFloat = 10
    

    
    
    override func collectionViewContentSize() -> CGSize {
        //let statusBarHeight =  UIApplication.sharedApplication().statusBarFrame.size.height as CGFloat
        //let navBarHeight = self.parentViewController.navigationController.navigationBar.frame.size.height
        //let navBarHeight = 32 as CGFloat
        let barHeight = collectionView?.contentInset.top
        let contentHeight = collectionView!.bounds.size.height - barHeight!
        //let contentHeight = collectionView!.frame.height - statusBarHeight
        let contentWidth = collectionView!.bounds.size.width
        return CGSizeMake(contentWidth, contentHeight)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutAttributesForElementsInRect(rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var layoutAttributes = [UICollectionViewLayoutAttributes]()
        
        // Cells
        let visibleIndexPaths = indexPathsOfItemsInRect(rect)
        print("Visible cells: \(visibleIndexPaths.count)")
        layoutAttributes += visibleIndexPaths.map {
            self.layoutAttributesForItemAtIndexPath($0)
        }
        
        // Supplementary views
        let secondHeaderViewIndexPaths = indexPathsOfSecondHeaderViewsInRect(rect)
        layoutAttributes += secondHeaderViewIndexPaths.map {
            self.layoutAttributesForSupplementaryViewOfKind(PulseHeaderType.Second.rawValue, atIndexPath: $0)
        }
        
        let channelHeaderViewIndexPaths = indexPathsOfChannelHeaderViewsInRect(rect)
        layoutAttributes += channelHeaderViewIndexPaths.map {
            self.layoutAttributesForSupplementaryViewOfKind(PulseHeaderType.Channel.rawValue, atIndexPath: $0)
        }
        
        //Finish Line : only one instance
//        layoutAttributes += [layoutAttributesForSupplementaryViewOfKind(FinishLineType, atIndexPath: NSIndexPath(index: 1))]
        
        
        // Decoration Views
        let verticalGridViewIndexPaths = indexPathsOfVerticalGridLineViewsInRect(rect)
        layoutAttributes += verticalGridViewIndexPaths.map {
            self.layoutAttributesForDecorationViewOfKind(GridLineType.Vertical.rawValue, atIndexPath: $0)
        }
        let horizontalGridViewIndexPaths = indexPathsOfHorizontalGridLineViewsInRect(rect)
        layoutAttributes += horizontalGridViewIndexPaths.map {
            self.layoutAttributesForDecorationViewOfKind(GridLineType.Horizontal.rawValue, atIndexPath: $0)
        }
        
        return layoutAttributes
    }
    
    override func layoutAttributesForItemAtIndexPath(indexPath: NSIndexPath) -> (UICollectionViewLayoutAttributes!) {
        let attributes = UICollectionViewLayoutAttributes(forCellWithIndexPath: indexPath)
        if let event = (getCellModel(indexPath) as? PulseEventCellModel)?.event {
            attributes.frame = frameForEvent(event)
        }
        //print(indexPath.row); print(": ")
        //print(attributes.frame)
        return attributes
    }
    
    override func layoutAttributesForSupplementaryViewOfKind(elementKind: String, atIndexPath indexPath: NSIndexPath) -> (UICollectionViewLayoutAttributes!) {
        let attributes = UICollectionViewLayoutAttributes(forSupplementaryViewOfKind: elementKind, withIndexPath: indexPath)
        
        let totalHeight = collectionViewContentSize().height
        let totalWidth  = collectionViewContentSize().width

        if elementKind == PulseHeaderType.Second.rawValue {
            let availableWidth =  totalWidth - ChannelHeaderWidth
            let widthPerSecond = availableWidth / SecondsMaximum
            attributes.frame = CGRectMake(ChannelHeaderWidth + widthPerSecond * (CGFloat(indexPath.item) - 0.5), 0, widthPerSecond, SecondHeaderHeight)
            attributes.zIndex = -10
        } else if elementKind == PulseHeaderType.Channel.rawValue {
            let availableHeight = totalHeight - SecondHeaderHeight
            let heightPerChannel = availableHeight / ChannelsMaximum
            attributes.frame = CGRectMake(0, SecondHeaderHeight + (heightPerChannel * CGFloat(indexPath.item)), ChannelHeaderWidth, heightPerChannel)
            attributes.zIndex = -10
//        } else if elementKind ==  FinishLineType {
//            let availableWidth =  totalWidth - ChannelHeaderWidth
//            let widthPerSecond = availableWidth / SecondsMaximum
//            attributes.frame = CGRectMake(ChannelHeaderWidth + widthPerSecond * finishTime - finishSize / 2, totalHeight - finishSize / 2, finishSize, finishSize)
            
        }
        return attributes
    }
    
    
    override func layoutAttributesForDecorationViewOfKind(elementKind: String, atIndexPath indexPath: NSIndexPath) -> (UICollectionViewLayoutAttributes!) {
        let attributes = UICollectionViewLayoutAttributes(forDecorationViewOfKind: elementKind, withIndexPath: indexPath)
        
        let totalHeight = collectionViewContentSize().height
        let totalWidth  = collectionViewContentSize().width
        
        if elementKind == GridLineType.Vertical.rawValue {
            let availableWidth =  totalWidth - ChannelHeaderWidth
            let widthPerSecond = availableWidth / SecondsMaximum
            attributes.frame = CGRectMake(ChannelHeaderWidth + widthPerSecond * CGFloat(indexPath.item), SecondHeaderHeight, 1, totalHeight - SecondHeaderHeight)
            attributes.zIndex = -11
        } else if elementKind == GridLineType.Horizontal.rawValue {
            let availableHeight = totalHeight - SecondHeaderHeight
            let heightPerChannel = availableHeight / ChannelsMaximum
            attributes.frame = CGRectMake(0.0, SecondHeaderHeight + (heightPerChannel * CGFloat(indexPath.item)), totalWidth, 1)
            attributes.zIndex = -11
        }
        return attributes
    }
    

    
    override func shouldInvalidateLayoutForBoundsChange(newBounds: CGRect) -> Bool {
        // optimize to return true only when really needed and not when scrolling
//        var oldBounds : CGRect = self.collectionView.bounds
//        if newBounds.width != oldBounds.width return true
//        return false
        return true
    }

}

extension PulseLayout {
    func indexPathsOfEventsBetweenMinSecondIndex(minChannelIndex: Int, maxChannelIndex: Int, minStartSecond: CGFloat, maxStartSecond: CGFloat) -> [NSIndexPath] {
        var indexPaths = [NSIndexPath]()
        
        if let cellmodels = getCellModels(0) as? [PulseEventCellModel] {
            //print("Number of cell models saved: \(cellmodels.count)")
            for i in 0..<cellmodels.count {
                let event = cellmodels[i].event
                if event.channel >= minChannelIndex && event.channel <= maxChannelIndex{
                    //let K = 1.0
                    if event.startSecond - minStartSecond >= 0.0
                        && event.startSecond - maxStartSecond  <= 0.0
                     {
                        let indexPath = NSIndexPath(forItem: i, inSection: 0)
                        indexPaths.append(indexPath)
                    } else {
                        print("This cell is not visible: Index: \(cellmodels[i].indexPath.row); Channel: \(event.channel); Start: \(event.startSecond); Duration: \(event.durationInSeconds)")
                    }
                }
            }
        }
        return indexPaths
    }
    
    func indexPathsOfItemsInRect(rect: CGRect) -> [NSIndexPath] {
        let minVisibleChannel = channelIndexFromYCoordinate(CGRectGetMinY(rect))
        let maxVisibleChannel = channelIndexFromYCoordinate(CGRectGetMaxY(rect))
        let minVisibleSecond  = secondIndexFromXCoordinate(CGRectGetMinX(rect))
        let maxVisibleSecond  = secondIndexFromXCoordinate(CGRectGetMaxX(rect))

        return indexPathsOfEventsBetweenMinSecondIndex(minVisibleChannel, maxChannelIndex: maxVisibleChannel, minStartSecond: minVisibleSecond, maxStartSecond: maxVisibleSecond)
    }
    
    func channelIndexFromYCoordinate(yPosition: CGFloat) -> Int {
        
        
        let contentHeight = collectionViewContentSize().height - SecondHeaderHeight
        let HeightPerChannel = contentHeight / ChannelsMaximum
        
        let channelIndex = max(0, Int((yPosition - SecondHeaderHeight) / HeightPerChannel))
        //print("yPosition: \(yPosition)")
        //print("heightPerChannel: \(HeightPerChannel)")
        return channelIndex
    }
    
    func secondIndexFromXCoordinate(xPosition: CGFloat) -> CGFloat {
        let contentWidth = collectionViewContentSize().width - ChannelHeaderWidth
        let WidthPerSecond = contentWidth / SecondsMaximum
        
        let secondIndex = max(0, (xPosition - ChannelHeaderWidth) / WidthPerSecond)
        return secondIndex
    }
    
    func indexPathsOfSecondHeaderViewsInRect(rect: CGRect) -> [NSIndexPath] {
        if CGRectGetMinY(rect) > SecondHeaderHeight {
            return []
        }
        
        let minSecondIndex = Int(secondIndexFromXCoordinate(CGRectGetMinX(rect)))
        let maxSecondIndex = Int(secondIndexFromXCoordinate(CGRectGetMaxX(rect)))
        
        return (minSecondIndex...maxSecondIndex).map { index -> NSIndexPath in
            NSIndexPath(forItem: index, inSection: 0)
        }
    }
    
    func indexPathsOfChannelHeaderViewsInRect(rect: CGRect) -> [NSIndexPath] {
        if CGRectGetMinX(rect) > ChannelHeaderWidth {
            return []
        }
        
        let minChannelIndex = channelIndexFromYCoordinate(CGRectGetMinY(rect))
        let maxChannelIndex = channelIndexFromYCoordinate(CGRectGetMaxY(rect))
        
        return (minChannelIndex...maxChannelIndex).map { index -> NSIndexPath in
            NSIndexPath(forItem: index, inSection: 0)
        }
    }
    
    
    func indexPathsOfVerticalGridLineViewsInRect(rect: CGRect) -> [NSIndexPath] {
        if CGRectGetMaxX(rect) < SecondHeaderHeight {
            return []
        }
        
        let minSecondIndex = Int(secondIndexFromXCoordinate(CGRectGetMinX(rect)))
        let maxSecondIndex = Int(secondIndexFromXCoordinate(CGRectGetMaxX(rect)))
        
        return (minSecondIndex...maxSecondIndex).map { index -> NSIndexPath in
            NSIndexPath(forItem: index, inSection: 0)
        }
    }
    func indexPathsOfHorizontalGridLineViewsInRect(rect: CGRect) -> [NSIndexPath] {
        if CGRectGetMaxY(rect) < ChannelHeaderWidth {
            return []
        }
        
        let minChannelIndex = channelIndexFromYCoordinate(CGRectGetMinY(rect))
        let maxChannelIndex = channelIndexFromYCoordinate(CGRectGetMaxY(rect))
        
        return (minChannelIndex...maxChannelIndex).map { index -> NSIndexPath in
            NSIndexPath(forItem: index, inSection: 0)
        }
    }
    
    func frameForEvent(event: PulseEvent) -> CGRect {
        let totalHeight = collectionViewContentSize().height - SecondHeaderHeight
        let HeightPerChannel = totalHeight / ChannelsMaximum
        
        let contentWidth = collectionViewContentSize().width - ChannelHeaderWidth
        let WidthPerSecond = contentWidth / SecondsMaximum
        
        var frame = CGRectZero
        frame.origin.x = ChannelHeaderWidth + WidthPerSecond * CGFloat(event.startSecond)
        frame.origin.y = SecondHeaderHeight + HeightPerChannel * CGFloat(event.channel)
        frame.size.height = HeightPerChannel
        frame.size.width = CGFloat(event.durationInSeconds) * WidthPerSecond
        //print("StartSecond:"); print(event.startSecond); print("; frameX: "); print(frame.origin)
        frame = CGRectInset(frame, 0.0, VerticalSpacing)
        return frame
    }
}


// For dragging Borders
//extension PulseLayout {
//    func updateRightBorderofCell(#indexPath: NSIndexPath, dragPoint: CGPoint) -> UICollectionViewLayoutAttributes {
//        var attributes = UICollectionViewLayoutAttributes(forCellWithIndexPath: indexPath)
//        var newFrame = attributes.frame
//        attributes.frame.size.width = dragPoint.x -  attributes.frame.origin.x
//
//        return attributes
//    }
//    
//    
//    func updateWidthBasedOnPinch(#indexPath: NSIndexPath, scale: CGFloat) -> UICollectionViewLayoutAttributes {
//        var attributes = UICollectionViewLayoutAttributes(forCellWithIndexPath: indexPath)
//        var newFrame = attributes.frame
//        attributes.frame.size.width = max(attributes.frame.size.width * scale, 0.1)
//        
//        return attributes
//    }
//}

