//
//  PulseEventCell.swift
//  Example
//
//  Created by Le VanNghia on 6/29/15.
//  Copyright (c) 2015 Le Van Nghia. All rights reserved.
//

import UIKit
import Sapporo

class PulseEventCellModel: SACellModel {
    let event: PulseEvent
    
    init(event: PulseEvent, selectionHandler: SASelectionHandler) {
        self.event = event
        super.init(cellType: PulseEventCell.self, selectionHandler: selectionHandler)
    }
}

class PulseEventCell: SACell {
    @IBOutlet var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = 0
        layer.borderWidth = 1.0
        layer.borderColor = UIColor(red: 0, green: 0, blue: 0.7, alpha: 1).CGColor
    }
    
    override func configure(cellmodel: SACellModel) {
        super.configure(cellmodel)
        
        if let _ = cellmodel as? PulseEventCellModel {
            titleLabel.text = "" //cellmodel.event.title
        }
    }
    
    override func prepareForReuse() {
        backgroundColor = UIColor(red: 164/255, green: 215/255, blue: 255/255, alpha: 0.7)
    }
    
//    func setSelected(selected: Bool) {
//        if selected { self.backgroundColor = UIColor(red: 164/255, green: 215/255, blue: 255/255, alpha: 1.0) }
//        else { self.backgroundColor = UIColor(red: 164/255, green: 215/255, blue: 255/255, alpha: 0.7) }
//
//    }
//    override func didHighlight(collectionView: UICollectionView) {
//        self.backgroundColor = UIColor(red: 58/255, green: 141/255, blue: 225/255, alpha: 1.0)
//    }
//    override func didUnhighlight(collectionView: UICollectionView) {
//        self.backgroundColor = UIColor(red: 164/255, green: 215/255, blue: 255/255, alpha: 0.7)
//        
//    }
}