//
//  HistoryTableViewController.swift
//  TrainFES
//
//  Created by Cristian Duguet on 9/17/15.
//  Copyright (c) 2015 TrainFES. All rights reserved.
//

import UIKit

class HistoryTableViewController: UITableViewController, RFduinoDelegate {

    
    var history = [String]?()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        history =  (NSUserDefaults.standardUserDefaults().objectForKey("sequenceHistory") as? [String])
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return history?.count ?? 0
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) 

        // Configure the cell...
        cell.textLabel?.text = history?[indexPath.item] ?? ""
        
        return cell
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)

        sendString(history?[indexPath.item] ?? "")
    }

    override func viewWillAppear(animated: Bool) {
        history =  (NSUserDefaults.standardUserDefaults().objectForKey("sequenceHistory") as? [String])
        print(history)
        self.tableView.reloadData()
    }
    
    
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    
    // MARK: RFDuino Functions
    
    func sendByte(byte: UInt8) {
        var tx: [UInt8] = [byte]
        let data = NSData(bytes: &tx, length: sizeof(UInt8))
        do {
            try rfduino.send(data)
        } catch _ {
        }
    }
    
    func sendString(str: String) {
        var byteArray = [UInt8]()
        for char in str.utf8{
            byteArray += [char]
        }
        let data = NSData(bytes: &byteArray, length: byteArray.count)
        
        do {
            try rfduino.send(data)
        } catch _ {
        }
    }
    
    func disconnect(sender: String) {
        print("Disconnecting...")
        rfduino.disconnect()
    }
    
    func didReceive(data: NSData!) {
        print("Received Data")
        //        var value : [UInt8] = [0]; data.getBytes(&value, length: 1)
        //        var len = data.length
        //
        //        if (value[0] == 1) { image1.image = UIImage(named: "on")}
        //        else { image1.image = UIImage(named: "off")}
    }
    
    

}
