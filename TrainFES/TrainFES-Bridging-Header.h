//
//  TrainFES-Bridging-Header.h.h
//  TrainFES
//
//  Created by Cristian Duguet on 9/16/15.
//  Copyright (c) 2015 TrainFES. All rights reserved.
//

#ifndef TrainFES_TrainFES_Bridging_Header_h_h
#define TrainFES_TrainFES_Bridging_Header_h_h

#import "ScanViewController.h"
//#import "rfduino/RFduinoManagerDelegate.h"
#import "RfduinoManager.h"
#import "RFduino.h"
#import "RFduinoDelegate.h"
//#import "rfduino/CustomCellBackground.h"

#endif
