//
//  SeparatorView.swift
//  Dynamic Collection View
//
//  Created by Cristian Duguet on 8/29/15.
//  Copyright (c) 2015 CrowdTransfer. All rights reserved.
//

import Foundation
import UIKit

class GridLineView: UICollectionReusableView {
    required init(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)!
        self.backgroundColor = UIColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 1)
    }
    
    override init(frame:CGRect) {
        super.init(frame:frame)
        self.backgroundColor = UIColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 1)
    }
    override  func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 1)

    }
}