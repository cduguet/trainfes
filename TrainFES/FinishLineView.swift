//
//  finishLineView.swift
//  TrainFES
//
//  Created by Cristian Duguet on 9/7/15.
//  Copyright (c) 2015 TrainFES. All rights reserved.
//

import Foundation
import UIKit

class FinishLineView: UICollectionReusableView {
    
    var finishTime = 5.0
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)!
        self.backgroundColor = UIColor(red: 1.0, green: 0.0, blue: 0.0, alpha: 1)
    }
    
    override init(frame:CGRect) {
        super.init(frame:frame)
        self.backgroundColor = UIColor(red: 1.0, green: 0.0, blue: 0.0, alpha: 1)
    }
    override  func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = 5
        self.backgroundColor = UIColor(red: 1.0, green: 0.0, blue: 0.0, alpha: 1)
    }
}

