//
//  TestViewController.swift
//  Dynamic Collection View
//
//  Created by Cristian Duguet on 8/31/15.
//  Copyright (c) 2015 CrowdTransfer. All rights reserved.
//

import UIKit

let channels  = (1...6).map{$0} as [Int]
let seconds = (1...10).map{$0} as [Int]

var intensities = [Int](count:6, repeatedValue: 0)
//[0, 0, 0, 0, 0, 0] as [Int]

//RFduino
var rfduino = RFduino()


class TestViewController: UIViewController, UIPickerViewDelegate, RFduinoDelegate, ScanDelegate, UINavigationControllerDelegate{
    @IBOutlet var channelLabel: UILabel!
    @IBOutlet var secondsLabel: UILabel!
    @IBOutlet var channelPickerView: UIPickerView!
    @IBOutlet var secondsPickerView: UIPickerView!
    @IBOutlet var intensityLabel: UILabel!
    @IBOutlet var intensitySlider: UISlider!
    
    @IBOutlet var sendButton: UIButton!
    @IBOutlet var PulseButton: UIBarButtonItem!
    
    
    // Channel Intensities
    
    @IBOutlet var i1: UILabel!
    @IBOutlet var i2: UILabel!
    @IBOutlet var i3: UILabel!
    @IBOutlet var i4: UILabel!
    @IBOutlet var i5: UILabel!
    @IBOutlet var i6: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Add right navbar buttons
        let bluetoothButtonItem: UIBarButtonItem =  UIBarButtonItem(image: UIImage(named:"familia_dos_bluetooth"), landscapeImagePhone: UIImage(named:"familia_dos_bluetooth"), style: UIBarButtonItemStyle.Plain, target: self, action: "bluetoothPressed:")
        
        let settingsButtonItem: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "familia_dos_ajustes"), style: .Plain, target: self, action: "settingsPressed:")
        
        
        let newSequenceButtonItem: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "familia_dos_masmas"), style: .Plain, target: self, action: "newSequencePressed:")
        
        
        self.navigationItem.setRightBarButtonItems([PulseButton, newSequenceButtonItem, settingsButtonItem, bluetoothButtonItem], animated: true)
        
        // PickerView delegates
        channelPickerView.delegate = self
        secondsPickerView.delegate = self
        
        //Intensities
        i1.text = "\(intensities[0])"
        i2.text = "\(intensities[1])"
        i3.text = "\(intensities[2])"
        i4.text = "\(intensities[3])"
        i5.text = "\(intensities[4])"
        i6.text = "\(intensities[5])"
        
        //RFduino
        rfduino.delegate = self
    }
    
    // MARK: Menu Buttons
    
    // Bluetooth
    func bluetoothPressed(sender:UIButton) {
        // Disconnect the current bluetooth Connection, if any
        rfduino.disconnect()
        
        //Instantiate ScanViewController using a Delegate
        let scanController: ScanViewController = ScanViewController(style: .Plain)
        let navController: UINavigationController = UINavigationController(rootViewController: scanController)
        //
        navController.navigationBar.tintColor = UIColor.blackColor()
        navController.delegate = self
        scanController.myDelegate = self
        navController.modalTransitionStyle =  UIModalTransitionStyle.CrossDissolve
        self.presentViewController(navController, animated: true, completion: nil)
    }
    // Settings
    func settingsPressed (sender:UIButton) {
        performSegueWithIdentifier("settingsSegue", sender: self)
    }
    // New Sequence
    func newSequencePressed(sender:UIButton) {
        let alert = UIAlertController(title: "Add New Sequence",
            message: nil,
            preferredStyle: UIAlertControllerStyle.Alert)
        
        let send = UIAlertAction(title: "Send", style: UIAlertActionStyle.Default) { action in
            let sequence = alert.textFields![0] 
            self.sendString(sequence.text!)
            print(sequence.text)
            self.saveSequence(sequence.text!)
        }
        
        send.enabled = false
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (_) in }
        
        alert.addTextFieldWithConfigurationHandler { (textField) in
            textField.placeholder = "_B(C);(i);(D);(F);(Ri);(Rf)\n"
            NSNotificationCenter.defaultCenter().addObserverForName(UITextFieldTextDidChangeNotification, object: textField, queue: NSOperationQueue.mainQueue()) { (notification) in
                send.enabled = textField.text != ""
            }
        }
        alert.addAction(send)
        alert.addAction(cancelAction)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func saveSequence(seq:String) {
        var history =  NSUserDefaults.standardUserDefaults().objectForKey("sequenceHistory") as? [String] ?? [String]()
        history.append(seq)
        print(history)
        NSUserDefaults.standardUserDefaults().setObject(history, forKey: "sequenceHistory")
    }


    
    @IBAction func pulseButtonPressed(sender: AnyObject) {
        if intensities == [0,0,0,0,0,0] {
            
            let alertController = UIAlertController(title: NSLocalizedString("Zero Intensities", comment: "Zero Intensities Alert Title"),
                message: NSLocalizedString("Please set up at least one intensity different from zero.", comment: "Zero Intensities Alert Message"),
                preferredStyle: UIAlertControllerStyle.Alert);
            
            alertController.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "OK"), style: UIAlertActionStyle.Default,handler: nil))
            self.presentViewController(alertController, animated: true, completion: nil)
            return
        } else if !rfduino.isConnected() {
            let alertController = UIAlertController(title: NSLocalizedString("Not Connected to Device", comment: "Not Connected Alert Title"),
                message: NSLocalizedString("Please first connect to a device by pressing the Bluetooth icon", comment: "Not Connected Alert Message"),
                preferredStyle: UIAlertControllerStyle.Alert);
            
            alertController.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "OK"), style: UIAlertActionStyle.Default,handler: nil))
            self.presentViewController(alertController, animated: true, completion: nil)
            return
        }
        performSegueWithIdentifier("pulseSegue", sender: self)
    }
    
    
    // MARK: RFDuino Functions
    
    func sendByte(byte: UInt8) {
        var tx: [UInt8] = [byte]
        let data = NSData(bytes: &tx, length: sizeof(UInt8))
        do {
            try rfduino.send(data)
        } catch _ {
        }
    }
    
    func sendString(str: String) {
        var byteArray = [UInt8]()
        for char in str.utf8{
            byteArray += [char]
        }
        let data = NSData(bytes: &byteArray, length: byteArray.count)
        
        do {
            try rfduino.send(data)
        } catch _ {
        }
    }

    func disconnect(sender: String) {
        print("Disconnecting...")
        rfduino.disconnect()
    }
    
    func didReceive(data: NSData!) {
        print("Received Data")
        //        var value : [UInt8] = [0]; data.getBytes(&value, length: 1)
        //        var len = data.length
        //
        //        if (value[0] == 1) { image1.image = UIImage(named: "on")}
        //        else { image1.image = UIImage(named: "off")}
    }
    
    // MARK: ScanDelegate Methods
    
    func ScanViewControllerDismissed(rfd: RFduino!) {
        rfduino = rfd
        print("selected rfduino: \(rfduino)")
    }

    
    // MARK: UI Elements 
    
    @IBAction func intensitySliderChanged(sender: AnyObject) {
        let row =   channelPickerView.selectedRowInComponent(0)
        let picked = channels[row]
        
        switch picked {
        case 1: i1.text = "\(Int(intensitySlider.value))"
        case 2: i2.text = "\(Int(intensitySlider.value))"
        case 3: i3.text = "\(Int(intensitySlider.value))"
        case 4: i4.text = "\(Int(intensitySlider.value))"
        case 5: i5.text = "\(Int(intensitySlider.value))"
        case 6: i6.text = "\(Int(intensitySlider.value))"
        default: print("error: no channel selected")
        }
    }
    @IBAction func intensitySliderEndedChanging(sender: AnyObject) {
        print("ended slider")
        intensities[0] = Int(i1.text!)!
        intensities[1] = Int(i2.text!)!
        intensities[2] = Int(i3.text!)!
        intensities[3] = Int(i4.text!)!
        intensities[4] = Int(i5.text!)!
        intensities[5] = Int(i6.text!)!
    }
    
    
    @IBAction func sendButtonPressed(sender: AnyObject) {
        var row =   channelPickerView.selectedRowInComponent(0)
        let channel = channels[row]
        let intensity = intensities[row]
        row = secondsPickerView.selectedRowInComponent(0)
        let second = seconds[row]
        
        print("Channel \(channel); Seconds:\(second); Intensity:\(intensity)")
        
        
        var configStrings = [String]()
        for var i=0; i<6; i++ {
            configStrings.append("_A\(i+1);\(intensities[i]);\(pulseWidth);\(frequency);\(second)\n")
            print(configStrings.last!, terminator: "")
            sendString(configStrings.last!)
        }
    }
    
    // MARK: For PickerView
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int  {
        return 1
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == channelPickerView {
            return channels.count
        } else if pickerView == secondsPickerView {
            return seconds.count
        }
        return 1
    }
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView ==  channelPickerView {
            return "\(channels[row])"
        } else if pickerView == secondsPickerView {
            return "\(seconds[row])"
        }
        return ""
    }
    
    // MARK: ViewController Helpers
    override func viewWillDisappear(animated: Bool) {
        intensities[0] = Int(i1.text!)!
        intensities[1] = Int(i2.text!)!
        intensities[2] = Int(i3.text!)!
        intensities[3] = Int(i4.text!)!
        intensities[4] = Int(i5.text!)!
        intensities[5] = Int(i6.text!)!
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "pulseSegue" {
            let pulseVC = segue.destinationViewController as! PulseViewController
            pulseVC.hidesBottomBarWhenPushed = true
        }
    }


}
