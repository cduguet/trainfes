//
//  SettingsViewController.swift
//  TrainFES
//
//  Created by Cristian Duguet on 9/11/15.
//  Copyright (c) 2015 TrainFES. All rights reserved.
//

import UIKit



var pulseWidth = 20 as Int
var frequency  =  5 as Int
var leftSlopeDuration  = 0 as Int
var rightSlopeDuration = 0 as Int


class SettingsViewController: UIViewController {
    
    @IBOutlet var pulseWidthSlider: UISlider!
    @IBOutlet var frequencySlider: UISlider!
    @IBOutlet var leftSlopeSlider: UISlider!
    @IBOutlet var rightSlopeSlider: UISlider!
    
    
    @IBOutlet var pulseWidthValueLabel: UILabel!
    @IBOutlet var frequencyValueLabel: UILabel!
    @IBOutlet var leftSlopeValueLabel: UILabel!
    @IBOutlet var rightSlopeValueLabel: UILabel!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        pulseWidthValueLabel.text = "\(pulseWidth)"
        frequencyValueLabel.text = "\(frequency)"
        leftSlopeValueLabel.text = "\(leftSlopeDuration)"
        rightSlopeValueLabel.text = "\(rightSlopeDuration)"
        
        pulseWidthSlider.value = Float(pulseWidth)
        frequencySlider.value = Float(frequency)
        leftSlopeSlider.value = Float(leftSlopeDuration)
        rightSlopeSlider.value = Float(rightSlopeDuration)
    }

    @IBAction func pulseWidthSliderChanged(sender: AnyObject) {
        pulseWidth = Int(pulseWidthSlider.value)
        pulseWidthValueLabel.text = "\(pulseWidth)"
    }
    
    @IBAction func FrequencySliderChanged(sender: AnyObject) {
        frequency = Int(frequencySlider.value)
        frequencyValueLabel.text = "\(frequency)"
    }
    
    @IBAction func leftSlopeSliderChanged(sender: AnyObject) {
        leftSlopeDuration = Int(leftSlopeSlider.value)
        leftSlopeValueLabel.text = "\(leftSlopeDuration)"
    }
    
    @IBAction func rightSlopeSliderChanged(sender: AnyObject) {
        rightSlopeDuration = Int(rightSlopeSlider.value)
        rightSlopeValueLabel.text = "\(rightSlopeDuration)"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
