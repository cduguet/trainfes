//
//  PulseEvent.swift
//  Example
//
//  Created by Cristian Duguet on 6/29/15.
//  Copyright (c) 2015 Cristian Duguet . All rights reserved.
//

import Foundation
import UIKit

class PulseEvent {
    let title            : String
    var channel          : Int
    var startSecond      : CGFloat
    var durationInSeconds: CGFloat
    var leftSlope        : Bool
    var rightSlope       : Bool
    
    init(title: String, channel: Int, startSecond: CGFloat, durationInSeconds: CGFloat, leftSlope: Bool = false, rightSlope: Bool = false) {
        self.title = title
        self.channel = channel
        self.startSecond = startSecond
        self.durationInSeconds = durationInSeconds
        self.leftSlope = leftSlope
        self.rightSlope = rightSlope
    }
    
}